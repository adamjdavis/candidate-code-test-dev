# OneAmerica Code Test

Please see the image in this repository titled `codetest-screenshot.png`.

- You should fork this repository and then submit a pull request in this
  repository for your HTML, CSS, and JavaScript to recreate the screenshot and
  meet the requirements below.
- Please use the following libraries: BackboneJS and MarionetteJS.
- You may also use jQuery and Bootstrap as needed. You may use other libraries
  in addition to these.
- You should submit a single HTML file, but you may include additional CSS and
  JavaScript files.
- Write your code assuming that this is a single page web application.
- There should be a model for a 'user', consisting of an id, username, first and
  last names, and email address.
- There should also be a collection for 'alerts', each of which should be a
  model, consisting of an id and message.
- Clicking on either the text `Alerts` should open a new view, containing the
  text `This is the view for Alerts` if the `Alerts` was clicked. The view
  should also contain all the existing alerts, and enable new alerts to be
  added, and old alerts to be deleted.
- Clicking on the text `Username` should open a modal dialog, containing the
  text `This is the dialog for the user`. The dialog should contain all
  properties present in the user model, and the ability to edit the properties
  except for the id.
- The view and model should both have a single button that closes the view or
  modal when clicked, and returns the UI to the main view.
- You may use an external system to mock an API, or files in the local
  filesystem.
- You may include any other code to run a local server for viewing your file,
  code to run unit tests, or any other additional tooling you wish to include.
- Be prepared to explain your choices in a follow up interview session.

When submitting your pull request, please do the following:

- Make sure your repository is public so reviewers can clone it for testing.
- Include in your PR description any steps needed to run your code locally.
